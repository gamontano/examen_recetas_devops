Esta receta mexicana tradicional de Auténtico mole negro oaxaqueño de Oaxaca, México lleva los siguientes ingredientes:

1 guajolote grande
7 kilo jitomate
2 kilo manteca de cerdo
1 1/2 kilo carne de cerdo
1 kilo chiles secos
1 kilo ajonjolí
1 kilo plátano frito
1/4 kilo chocolate
1/2 kilo nuez pelada
1/4 kilo almendras
1/4 kilo cacahuate frito
1/4 kilo pasas
100 g azúcar
15 panes chicos de manteca
10 tomates verdes
6 clavos de olor
6 pimientas negras
4 hojas de aguacate
3 rajas de canela
2 cabezas grandes de ajo
2 cebollas grandes
1 cucharada de semilla de cilantro
1 cucharadita de cominos y de tomillo
1 cucharadita de orégano
1 pedacito chico de nuez moscada
1 pedacito chico de jengibre
1 tortilla tostada
ajos y cebollas para cocer el guajolote
sal
Para preparar auténtico mole negro oaxaqueño hay que cocer el guajolote, cortado en piezas, con ajos y cebolla. Desvenar y tostar los chiles (chilhuacle, ancho, negro, pasilla mexicano y chipoüe) hasta que se pongan negritos; quemar la tortilla, tostar las semillas de los chiles y luego lavarlas. Lavar los chiles en agua caliente y remojarlos luego en agua fría por espacio de media hora. Cortar los plátanos en trozos, rebanar la cebolla, pelar los dos ajos y freír todo junto con el pan, ajonjolí, cacahuates, almendras, nueces y pasas. Por separado se pone a hervir el jitomate rebanado junto con el tomate verde. Moler los chiles con todos los ingredientes y especias; moler el jitomate y tomate por separado. Freír la carne de cerdo en una cazuela de barro con suficiente manteca; agregar el jitomate; dejar al fuego durante una hora. Añadir el mole, el caldo y la carne del guajolote. Agregar, por último, chocolate, azúcar y las hojas de aguacate enteras, sal al gusto y dejar sazonar una hora más (mover de vez en cuando para que el guiso no se pegue a la cazuela). Servir luego. La receta alcanza para 35 raciones.



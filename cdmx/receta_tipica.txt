PAMBAZO

Ingredientes
4 Porciones

Seleccionar todos los ingredientes
7 chiles guajillos sin rabo ni semilla
1/4 de taza de cebolla picada
1 diente de ajo
1/4 de taza de agua
350 gramos de chorizo
3 papas mediana
8 piezas de pan telera o bolillo
1/2 taza de lechuga desmenuzada
1/4 de taza de crema agria

Preparaci�n
Agrega los chiles en una cacerola peque�a. Agrega suficiente agua para cubrirlos. Hazlos hervir; coc�nalos a fuego medio durante 10 minutos o hasta que est�n tiernos. Escurre los chiles; p�salos a la licuadora. Agrega la cebolla, el ajo, el agua; pon la tapa. L�cua esto hasta que est� homog�neo. P�salo por un colador de metal. Desecha los s�lidos que colaste; pon la salsa a un lado.
Cocina el chorizo y las papas, revolvi�ndolos de vez en cuando, en una sart�n grande a fuego medio durante 12 minutos o hasta que el chorizo est� bien cocido y las papas tiernas; esc�rrelos. Coloca esto en cantidades parejas en la mitad inferior de los panecillos; completa los pambazos con la mitad superior del pan. Ponlos a un lado.
Incorpora los s�ndwiches; coc�nalos 3 minutos o hasta que la salsa se consuma y ambos lados de los s�ndwiches est�n ligeramente tostados, pincel�ndolos de vez en cuando con el resto de la salsa. Completa el s�ndwich con la lechuga y la crema agria y s�rvelo.

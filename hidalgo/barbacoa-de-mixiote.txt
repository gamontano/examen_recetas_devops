Receta mexicana de Barbacoa de mixiote del Estado de Hidalgo, México.

40 cuadros de mixiote, remojados y escurridos
4 kilo carne de carnero (en trozos de 100 g)
250 g chiles anchos
150 g almendras
1 litro de caldo
1 cucharada de orégano
40 hojas de aguacate
10 dientes de ajo
3 cebollas
sal
Para preparar barbacoa de mixiote hay que tostar, desvenar y remojar los chiles en caldo caliente. Molerlos con ajo, cebolla, almendras sin cáscara, sal y orégano. Colocar en cada cuadro de mixiote una hoja de aguacate y una pieza de carnero bañada de salsa de chile. Amarrar los mixiotes con un hilo y formar bolsitas; cocerlos en vaporera. La receta alcanza para 20 raciones.


